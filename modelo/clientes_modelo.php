<?php
    require_once 'conexion.php';
    class ApiClientes {
        static function agregarClientesMdl ($datos, $tabla) {
            $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_cliente,direccion,departamento,municipio,nit,nrc,telefono1,telefono2,categoria_cliente,dui,giro,fax,correo,porcentaje_retencion) VALUES (:nombre_cliente,:direccion,:departamento,:municipio,:nit,:nrc,:telefono1,:telefono2,:categoria_cliente,:dui,:giro,:fax,:correo,:porcentaje_retencion)");

            $stmt->bindParam(":nombre_cliente", $datos["nombre_cliente"], PDO::PARAM_STR);
            $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
            $stmt->bindParam(":departamento", $datos["departamento"], PDO::PARAM_STR);
            $stmt->bindParam(":municipio", $datos["municipio"], PDO::PARAM_STR);
            $stmt->bindParam(":nit", $datos["nit"], PDO::PARAM_STR);
            $stmt->bindParam(":nrc", $datos["nrc"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono1", $datos["telefono_1"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono2", $datos["telefono_2"], PDO::PARAM_STR);
            $stmt->bindParam(":categoria_cliente", $datos["categoria_cliente"], PDO::PARAM_STR);
            $stmt->bindParam(":dui", $datos["dui"], PDO::PARAM_STR);
            $stmt->bindParam(":giro", $datos["giro"], PDO::PARAM_STR);
            $stmt->bindParam(":fax", $datos["fax"], PDO::PARAM_STR);
            $stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);
            $stmt->bindParam(":porcentaje_retencion", $datos["porcentaje_retencion"], PDO::PARAM_INT);

            if($stmt->execute()){ 
                return "ok";
            }else{
                return $stmt->errorInfo();
            }

            $stmt->close();
            $stmt = null;
        } 

        static function listarClientes ($idCliente){
            if($idCliente === false){
                $stmt = Conexion::conectar() -> prepare("select clientes.id_cliente, clientes.nombre_cliente, depsv.nombre_departamento, munsv.nombre_municipio, clientes.nit, clientes.nrc, clientes.telefono1, clientes.direccion from clientes inner join depsv on clientes.departamento = depsv.id_dep inner join munsv on clientes.municipio = munsv.id_muni");
                $stmt -> execute();
            }else{
                $stmt = Conexion::conectar() -> prepare("select clientes.id_cliente, clientes.nombre_cliente, clientes.departamento, clientes.municipio, depsv.nombre_departamento, munsv.nombre_municipio, clientes.nit, clientes.nrc, clientes.telefono1, clientes.telefono2, clientes.direccion, clientes.categoria_cliente, clientes.dui, clientes.giro, clientes.fax, clientes.correo, clientes.porcentaje_retencion from clientes inner join depsv on clientes.departamento = depsv.id_dep inner join munsv on clientes.municipio = munsv.id_muni where id_cliente = ".$idCliente);
                $stmt -> execute();
            }
            
            return $stmt -> fetchAll();
          }

          static function EliminarCliente($id){
              $stmt = Conexion::conectar() -> prepare("DELETE FROM clientes WHERE id_cliente = ".$id);
              if($stmt -> execute()){
                return $stmt -> errorInfo();
              }
              
          }

          static function listarCliente($item, $valor){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM clientes WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();
          }
    }
?>