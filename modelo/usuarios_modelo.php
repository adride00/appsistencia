<?php

require_once "conexion.php";

class ModeloUsuarios{

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/

	static public function mdlMostrarUsuarios($tabla, $item, $valor){


		if($item != null){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();
		}else{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$stmt -> execute();

			return $stmt -> fetchAll();
		}


		$stmt -> close();

		$stmt = null;

	}

	static function mdlIngresarUsuario ($tabla, $datos) {
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre,apellidos,correo,pass,tipo_usuario) VALUES (:nombre,:apellidos,:correo,:pass,:tipo_usuario)");

	$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
	$stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
	$stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);
	$stmt->bindParam(":pass", $datos["pass"], PDO::PARAM_STR);
	$stmt->bindParam(":tipo_usuario", $datos["tipo_usuario"], PDO::PARAM_STR);

	if($stmt->execute()){
		return "ok";
	}else{
		return "error";
	}

	$stmt->close();
	$stmt = null;
	}

	static function mdlEliminarUsuario($id,$tabla,$item){
		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $id, PDO::PARAM_STR);


		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;
	}

	static function mdlEditarUsuario($tabla,$datos){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, apellidos = :apellidos, correo = :correo, pass = :pass, tipo_usuario = :tipo_usuario WHERE id_usuario = :id_usuario");

			$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
			$stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
			$stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);
			$stmt->bindParam(":pass", $datos["pass"], PDO::PARAM_STR);
			$stmt->bindParam(":tipo_usuario", $datos["tipo_usuario"], PDO::PARAM_STR);
			$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_STR);

			if($stmt->execute()){
				return "ok";
			}else{
				return "error";
			}

			$stmt->close();
			$stmt = null;


		}
}
