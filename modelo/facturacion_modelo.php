<?php
    require_once 'conexion.php';
    class Api {
        static function agregarEmpleadoMdl ($datos, $tabla) {
            $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_empleado,apellidos_empleado,direccion,dui,nit,telefono,correo,salario,tipo_empleado) VALUES (:nombre_empleado,:apellidos_empleado,:direccion,:dui,:nit,:telefono,:correo,:salario,:tipo_empleado)");

            $stmt->bindParam(":nombre_empleado", $datos["nombre_empleado"], PDO::PARAM_STR);
            $stmt->bindParam(":apellidos_empleado", $datos["apellidos_empleado"], PDO::PARAM_STR);
            $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
            $stmt->bindParam(":dui", $datos["dui"], PDO::PARAM_STR);
            $stmt->bindParam(":nit", $datos["nit"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
            $stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);
            $stmt->bindParam(":salario", $datos["salario"], PDO::PARAM_STR);
            $stmt->bindParam(":tipo_empleado", $datos["tipo_empleado"], PDO::PARAM_STR);

            if($stmt->execute()){
                return "ok";
            }else{
                return $stmt->errorInfo();
            }

            $stmt->close();
            $stmt = null;
        } 

        static function listarProductos ($id, $producto){
            if($id === false){
                $stmt = Conexion::conectar() -> prepare("SELECT id_inventario, codigo, producto FROM inventario where producto like '%".$producto."%' or codigo like '%".$producto."%'");
                $stmt -> execute();
            }else{
                $stmt = Conexion::conectar() -> prepare("select  stock.stock, inventario.producto, inventario.id_inventario, inventario.precio from inventario inner join stock on inventario.id_inventario = stock.id_producto where inventario.id_inventario = ".$id);
                $stmt -> execute();
            }
            
            return $stmt -> fetchAll();
          }

          static function facturar($datos){
            
              $stmt = Conexion::conectar()->prepare("INSERT INTO factura(num_documento,id_cliente,total,sumas,iva,retenido,estado,tipo_pago,tipo_impresion,metodo_pago) VALUES(:num_documento,:id_cliente,:total,:sumas,:iva,:retenido,:estado,:tipo_pago,:tipo_impresion,:metodo_pago)");
              $stmt->bindParam(":num_documento", $datos["numero_documento"], PDO::PARAM_STR);
              $stmt->bindParam(":id_cliente", $datos["cliente"], PDO::PARAM_INT);
              $stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
              $stmt->bindParam(":sumas", $datos["sumas"], PDO::PARAM_STR);
              $stmt->bindParam(":iva", $datos["iva"], PDO::PARAM_STR);
              $stmt->bindParam(":retenido", $datos["retenido"], PDO::PARAM_STR);
              $stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
              $stmt->bindParam(":tipo_pago", $datos["tipo_pago"], PDO::PARAM_STR);
              $stmt->bindParam(":tipo_impresion", $datos["tipo_impresion"], PDO::PARAM_STR);
              $stmt->bindParam(":metodo_pago", $datos["metodo_pago"], PDO::PARAM_STR);
              
              if($stmt->execute()){
                $stmt2 = Conexion::conectar() -> prepare("SELECT id_factura FROM factura where num_documento =".$datos["numero_documento"]);
                $stmt2->execute();
                //$last = Conexion::conectar()->lastInsertId();
                return $stmt2 -> fetchAll();
            }else{
                return $stmt->errorInfo();
            }

            $stmt->close();
            $stmt = null;
          }
          
          static function facturarDetalle($idProductos,$descuento,$cantidad,$subtotales,$id_factura) {
            
              
                $stmt = Conexion::conectar() -> prepare("INSERT INTO factura_detalle(id_factura,id_producto,descuento,cantidad,subtotal) VALUES(:id_factura,:id_producto,:descuento,:cantidad,:subtotal)");
                $stmt->bindParam(":id_factura", $id_factura, PDO::PARAM_INT);
                $stmt->bindParam(":id_producto", $idProductos, PDO::PARAM_INT);
                $stmt->bindParam(":descuento", $descuento, PDO::PARAM_STR);
                $stmt->bindParam(":cantidad", $cantidad, PDO::PARAM_STR);
                $stmt->bindParam(":subtotal", $subtotales, PDO::PARAM_STR);
                if($stmt->execute()){
                    return "ok";
                }else{
                    return $stmt->errorInfo();
                }
    
                $stmt->close();
                $stmt = null;
              }
              
            
    }
?>