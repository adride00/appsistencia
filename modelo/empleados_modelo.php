<?php
    require_once 'conexion.php';
    class Api {
        static function agregarEmpleadoMdl ($datos, $tabla) {
            $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_empleado,apellidos_empleado,direccion,dui,nit,telefono,correo,salario,tipo_empleado) VALUES (:nombre_empleado,:apellidos_empleado,:direccion,:dui,:nit,:telefono,:correo,:salario,:tipo_empleado)");

            $stmt->bindParam(":nombre_empleado", $datos["nombre_empleado"], PDO::PARAM_STR);
            $stmt->bindParam(":apellidos_empleado", $datos["apellidos_empleado"], PDO::PARAM_STR);
            $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
            $stmt->bindParam(":dui", $datos["dui"], PDO::PARAM_STR);
            $stmt->bindParam(":nit", $datos["nit"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
            $stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);
            $stmt->bindParam(":salario", $datos["salario"], PDO::PARAM_STR);
            $stmt->bindParam(":tipo_empleado", $datos["tipo_empleado"], PDO::PARAM_STR);

            if($stmt->execute()){
                return "ok";
            }else{
                return $stmt->errorInfo();
            }

            $stmt->close();
            $stmt = null;
        } 

        static function listarEmpleados ($id){
            if($id === false){
                $stmt = Conexion::conectar() -> prepare("SELECT * FROM empleado");
                $stmt -> execute();
            }else{
                $stmt = Conexion::conectar() -> prepare("SELECT * FROM empleado WHERE id_empleado = ".$id);
                $stmt -> execute();
            }
            
            return $stmt -> fetchAll();
          }

          static function Eliminar($id){
              $stmt = Conexion::conectar() -> prepare("DELETE FROM empleado WHERE id_empleado = ".$id);
              if($stmt -> execute()){
                return $stmt -> errorInfo();
              }
              
          }

          static function listarCliente($item, $valor){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM clientes WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();
          }
    }
?>