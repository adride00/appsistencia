<?php
@session_start();
require '/var/www/html/andrade/modelo/clientes_modelo.php';
$apiObject = new ApiClientes();

if(isset($_POST['nombre_cliente'])){
    $datos = array('nombre_cliente' => $_POST['nombre_cliente'],
                       'departamento' => $_POST['departamento'],
                       'municipio' => $_POST['municipio'],
                       'nit' => $_POST['nit'],
                       'nrc' => $_POST['nrc'],
                       'telefono_1' => $_POST['telefono_1'],
                       'telefono_2' => $_POST['telefono_2'],
                       'direccion' => $_POST['direccion'],
                       'categoria_cliente' => $_POST['categoria_cliente'],
                       'porcentaje_retencion' => $_POST['porcentaje'],
                       'fax' => $_POST['fax'],
                       'dui' => $_POST['dui'],
                       'giro' => $_POST['giro'],
                       'correo' => $_POST['correo']);
    $tabla = 'clientes';
    $respuesta = $apiObject -> agregarClientesMdl($datos, $tabla);

}elseif($_GET['action'] == 'listarClientes'){
    if(isset($_GET['idCliente'])){
        $idCliente = $_GET['idCliente'];
        $respuesta = $apiObject -> listarClientes($idCliente);
    }else{
        $idCliente = false;
        $respuesta = $apiObject -> listarClientes($idCliente);
    }
    
}elseif($_GET['action'] == 'eliminarCliente'){
    $respuesta = $apiObject -> eliminarCliente($_GET['idCliente']);
}elseif($_GET['action'] == 'listarCliente'){
    $item = 'id_cliente';
    $respuesta = $apiObject -> listarCliente($item, $_GET['idCliente']);
}elseif(isset($_POST['editarNombre'])){
    $datos = array('editarNombre' => $_POST['editarNombre'],
                       'editarDepartamento' => $_POST['editarDepartamento'],
                       'editarMunicipio' => $_POST['editarMunicipio'],
                       'editarNit' => $_POST['editarNit'],
                       'editarNrc' => $_POST['editarNrc'],
                       'editarTelefono_1' => $_POST['telefeditarTelefono_1ono_1'],
                       'editarTelefono_2' => $_POST['editarTelefono_2'],
                       'editarDireccion' => $_POST['editarDireccion'],
                       'editarCategoria_cliente' => $_POST['editarCategoria_cliente'],
                       'editarPorcentaje_retencion' => $_POST['editarPorcentaje_retencion'],
                       'editarFax' => $_POST['editarFax'],
                       'editarDui' => $_POST['editarDui'],
                       'editarGiro' => $_POST['editarGiro'],
                       'editarCorreo' => $_POST['editarCorreo']);
    $tabla = 'clientes';
    $respuesta = $apiObject -> editarClientes($datos, $tabla);
}
echo json_encode($respuesta);
?>