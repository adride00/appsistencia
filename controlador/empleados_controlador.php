<?php

@session_start();
require '/var/www/html/andrade/modelo/empleados_modelo.php';
$apiObject = new Api();

if(isset($_POST['nombre_empleado'])){
    $datos = array('nombre_empleado' => $_POST['nombre_empleado'],
                       'apellidos_empleado' => $_POST['apellidos_empleado'],
                       'nit' => $_POST['nit'],
                       'telefono' => $_POST['telefono'],
                       'direccion' => $_POST['direccion'],
                       'tipo_empleado' => $_POST['tipo_empleado'],
                       'salario' => $_POST['salario'],
                       'dui' => $_POST['dui'],
                       'correo' => $_POST['correo']);
    $tabla = 'empleado';
    $respuesta = $apiObject -> agregarEmpleadoMdl($datos, $tabla);

}elseif($_GET['action'] == 'listarEmpleados'){
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $respuesta = $apiObject -> listarEmpleados($id);
    }else{
        $id = false;
        $respuesta = $apiObject -> listarEmpleados($id);
    }
    
}elseif($_GET['action'] == 'eliminarRegistro'){
    $respuesta = $apiObject -> eliminar($_GET['id']);
}elseif($_GET['action'] == 'listarCliente'){
    $item = 'id_cliente';
    $respuesta = $apiObject -> listarCliente($item, $_GET['idCliente']);
}elseif(isset($_POST['editarNombre'])){
    $datos = array('editarNombre' => $_POST['editarNombre'],
                       'editarDepartamento' => $_POST['editarDepartamento'],
                       'editarMunicipio' => $_POST['editarMunicipio'],
                       'editarNit' => $_POST['editarNit'],
                       'editarNrc' => $_POST['editarNrc'],
                       'editarTelefono_1' => $_POST['telefeditarTelefono_1ono_1'],
                       'editarTelefono_2' => $_POST['editarTelefono_2'],
                       'editarDireccion' => $_POST['editarDireccion'],
                       'editarCategoria_cliente' => $_POST['editarCategoria_cliente'],
                       'editarPorcentaje_retencion' => $_POST['editarPorcentaje_retencion'],
                       'editarFax' => $_POST['editarFax'],
                       'editarDui' => $_POST['editarDui'],
                       'editarGiro' => $_POST['editarGiro'],
                       'editarCorreo' => $_POST['editarCorreo']);
    $tabla = 'clientes';
    //$respuesta = $apiObject -> editarClientes($datos, $tabla);
}
echo json_encode($respuesta);
?>

