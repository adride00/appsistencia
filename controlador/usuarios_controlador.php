<?php
session_start();
require '/var/www/html/andrade/modelo/usuarios_modelo.php';


  class ControladorUsuarios{

    //Ingreso de usuario
    static public function ctrIngresoUsuario($user, $pass){

				$tabla = "usuarios";

				$item = "correo";

				//$encriptar = crypt($pass, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
				$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $user);

            if($respuesta["correo"] == $user && $respuesta["pass"] == $pass){

						$_SESSION["inicio"] = "ok";
						$_SESSION["tipo_usuario"] = $respuesta["tipo_usuario"];

						$_SESSION["nombre"] = $respuesta["nombre"];

            $_SESSION["id"] = $respuesta["id_usuario"];

						if($respuesta){
              $mensaje = 1;

            }
                    }else{

                        $mensaje = 0;
                    }
                    return $mensaje;
    }

    static function ctrMostrarUsuario ($item, $valor) {
      $tabla = "usuarios";
      $respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);
      return $respuesta;
    }

    static function ctrGuardarUsuario ($nombre,$apellidos,$correo,$pass,$tipo_usuario) {
      $tabla = 'usuarios';
      $datos = array("nombre" => $nombre,
                     "apellidos" => $apellidos,
                     "correo" => $correo,
                     "pass" => $pass,
                     "tipo_usuario" => $tipo_usuario);


      $respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);

      if($respuesta == "ok"){

      return  1;
      }else{
        return 0;

      }
    }

    static function ctrEliminarUsuario($id,$tabla,$item){
    $respuesta = ModeloUsuarios::mdlEliminarUsuario($id,$tabla,$item);
    if($respuesta == "ok"){
      return 1;
    }else{
      return 0;
    }
  }

  static function ctrEditarUsuario($nombre,$apellidos,$correo,$pass,$id_usuario,$tipo_usuario){
        //validar que sea texto y numero sin caracteres especiales

      $tabla = 'usuarios';
      $datos = array("nombre" => $nombre,
                    "apellidos" => $apellidos,
                    "correo" => $correo,
                    "pass" => $pass,
                    "id_usuario" => $id_usuario,
                    "tipo_usuario" => $tipo_usuario);


      $respuesta = ModeloUsuarios::mdlEditarUsuario($tabla, $datos);

          if($respuesta == "ok"){
            return  1;
          }else{
            return 0;
          }
  }
}

 ?>
