$(function(){

    $('#guardarUsuario').click(function(e){
      e.preventDefault();

      var nombre = $("#nombre").val()
      var apellidos = $("#apellidos").val()
      var correo = $("#correo").val()
      var pass = $("#pass").val()
      var tipo_usuario = $("#tipo_usuario").val()
      var formData = new FormData($('form')[0]);


        $.ajax({
          type: "POST",
          url: "./ajax/usuarios.php",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(res){
            if(res==1){


            $('#nombre').val('');
              $('#apellidos').val('');
              $('#correo').val('');
              $('#pass').val('');
              $('#modalAgregarUsuario').modal( 'hide' );
              swal.fire("Usuario Registrado Correctamente", "", "success");
              location.href="http://localhost/modulo6/index.php?ruta=usuarios";
            }
          }
        })

    });


  });


  $(".table").on("click",".btnEliminarUsuario",function(){


		var idUsuario = $(this).attr("idUsuarioEliminar");

		var datos = new FormData();
		datos.append("idEliminar", idUsuario);

    Swal.fire({
    title: 'Esta Seguro?',
    text: "Este Cambio es Irreversible!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'SI, Eliminarlo!'
  }).then((result) => {

    if (result.value) {
          $.ajax({
        		method: "post",
        		data: datos,
        		url: "./ajax/usuarios.php",
        		cache: false,
        		contentType: false,
        		processData: false,
        		success: function(res){

        			swal.fire("Eliminado!", "Docente ha sido eliminado.", "success");

              //$(".table").load('http://localhost/modulo6/index.php?ruta=docentes');
        			window.location.href = "http://localhost/modulo6/index.php?ruta=usuarios"
        		}
    	})
    }
  })

});


$(".table").on("click",".btnEditarUsuario",function(){


  var idUsuario = $(this).attr("idUsuario");

  var datos = new FormData();
  datos.append("idUsuario", idUsuario);

  $.ajax({
    url: './ajax/usuarios.php',
    method: 'POST',
    data: datos,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(respuesta){
      $("#id_usuario").val(respuesta["id_usuario"]);
      $("#editarNombre").val(respuesta["nombre"]);
      $("#editarApellidos").val(respuesta["apellidos"]);
      $("#editarCorreo").val(respuesta["correo"]);
      $("#editarPass").val(respuesta["pass"]);
      $("#select").html(respuesta["tipo_usuario"]);
  			$("#select").val(respuesta["tipo_usuario"]);

    }

  })
})

$('#editarUsuario').click(function(e){
    e.preventDefault();

    var formData = new FormData($('#formularioEditar')[0]);


    $.ajax({
    type: "post",
    url: "./ajax/usuarios.php",
    data: formData,
    contentType: false,
    processData: false,
    dataType: "json",
    success: function(res){

      if(res==1){


        $('#modalEditarUsuario').modal( 'hide' );
        swal.fire("Usuario Editado Correctamente", "", "success");
        location.href="http://localhost/modulo6/index.php?ruta=usuarios";
      }else{
      swal("error en los datos", "", "error");
      }
    }
    })

  });
