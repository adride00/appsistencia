


  export function enviarDatos(url) {
    var campos = document.querySelectorAll('.requeridos')
    var elementos = 0;
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].value == "") {

            campos[i].parentElement.lastElementChild.innerText = "Campo es requerido"
            campos[i].parentElement.lastElementChild.style = "color:red"
        } else {
            campos[i].parentElement.lastElementChild.innerText = ""

            elementos = elementos + 1;

        }

    }
    if (elementos == campos.length) {
        event.preventDefault()
        const data = new FormData(document.getElementById('form_general'));
        fetch(url, {
                method: 'POST',
                body: data
            })
            .then((response) => {
                return response.text()
            })
            .then((datos) => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
    }

}

