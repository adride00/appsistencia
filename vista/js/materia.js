$(function(){

    $('#guardarMateria').click(function(e){
      e.preventDefault();

      var nombre = $("#nombreMateria").val()
      var formData = new FormData($('form')[0]);


        $.ajax({
          type: "POST",
          url: "./ajax/materia.php",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(res){
            if(res==1){
            $('#nombreMateria').val('');

              $('#modalAgregarMateria').modal( 'hide' );
              swal.fire("Materia Registrada Correctamente", "", "success");
              location.href="http://localhost/modulo6/index.php?ruta=materias";
            }
          }
        })

    });


  });
  $('.btnEliminarMateria').click(function(e){
    var idMateria = $(this).attr("idMateriaEliminar");

    var datos = new FormData();
    datos.append("idEliminar", idMateria);

    Swal.fire({
    title: 'Esta Seguro?',
    text: "Este Cambio es Irreversible!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'SI, Eliminarlo!'
  }).then((result) => {

    if (result.value) {
          $.ajax({
            method: "post",
            data: datos,
            url: "./ajax/materia.php",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res){

              swal.fire("Eliminado!", "Materia ha sido eliminada.", "success");

              //$(".table").load('http://localhost/modulo6/index.php?ruta=docentes');
              window.location.href = "http://localhost/modulo6/index.php?ruta=materias"
            }
      })
    }
  })
});


$(".table").on("click",".btnEditarMateria",function(){


  var idMateria = $(this).attr("idMateria");

  var datos = new FormData();
  datos.append("idMateria", idMateria);

  $.ajax({
    url: './ajax/materia.php',
    method: 'POST',
    data: datos,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(respuesta){
      $("#id_materia").val(respuesta["id_materia"]);
      $("#editarNombreMateria").val(respuesta["nombre"]);

    }

  })
})

$('#editarMateria').click(function(e){
    e.preventDefault();

    var formData = new FormData($('#formularioEditar')[0]);


    $.ajax({
    type: "post",
    url: "./ajax/materia.php",
    data: formData,
    contentType: false,
    processData: false,
    dataType: "json",
    success: function(res){

      if(res==1){


        $('#modalEditarMateria').modal( 'hide' );
        swal.fire("Materia se Edito Correctamente", "", "success");
        location.href="http://localhost/modulo6/index.php?ruta=materias";
      }else{
      swal("error en los datos", "", "error");
      }
    }
    })

  });
