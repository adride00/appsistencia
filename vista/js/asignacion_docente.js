$(function(){

    $('#guardarAsignacion').click(function(e){
      e.preventDefault();

      var id_docente = $("#nombreDocente").val()
      var id_materia = $("#materia").val()
      var id_horario = $("#horario").val()
      var id_aula = $("#aula").val()
      var id_seccion = $("#seccion").val()

      var formData = new FormData($('form')[0]);



        $.ajax({
          type: "POST",
          url: "./ajax/asignacion_docente.php",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(res){
            if(res==1){

              swal.fire("Asignacion Registrada Correctamente", "", "success");
              location.href="http://localhost/modulo6/index.php?ruta=asignar_docente";
            }
          }
        })

    });


  });

  //SELECT docente.nombre as nombre_docente, materia.nombre as nombre_materia FROM docente INNER JOIN asignacion ON docente.id_docente = asignacion.id_asignacion INNER JOIN materia ON asignacion.id_materia = materia.id_materia
