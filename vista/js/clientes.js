function myFunction () {
    const retiene = document.getElementById('customCheckbox2')
    const porcentaje = document.querySelector('div#retiene_select')
    if(retiene.checked == true){
        porcentaje.hidden = false
        document.getElementById('porcentaje').classList.add("requeridos")
    }else{
        porcentaje.hidden = true
        document.getElementById('porcentaje').classList.remove("requeridos")
    }
}

     
    

document.getElementById('guardar').addEventListener('click', function () {
    var campos = document.querySelectorAll('.requeridos')
    var elementos = 0;
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].value == "") {

            campos[i].parentElement.lastElementChild.innerText = "Campo es requerido"
            campos[i].parentElement.lastElementChild.style = "color:red"
        } else {
            campos[i].parentElement.lastElementChild.innerText = ""

            elementos = elementos + 1;

        }

    }
    if (elementos == campos.length) {
        event.preventDefault()
        const data = new FormData(document.getElementById('form_general'));
        fetch('http://localhost/andrade/controlador/cliente_controlador.php', {
                method: 'POST',
                body: data
            })
            .then((response) => {
                return response.text()
            })
            .then((datos) => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
    }

});

window.addEventListener('load', () => {
    
    event.preventDefault()
    cargar()

});

function selecionarDep() {
    cargarMunicipios()
}

function cargar() {
    cargarDepartamentos()
        .then(() => {
            return cargarMunicipios()
        })

}

function cargarDepartamentos() {
    return new Promise(function (resolve) {
        fetch('http://localhost/andrade/controlador/municipios_controlador.php?action=listarDepartamentos')
            .then(response => {
                return response.json()
            })
            .then(data => {

                var municipios = document.getElementById('departamento');
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    var contenido = document.createTextNode(data[i].nombre_departamento);
                    option.appendChild(contenido);
                    municipios.appendChild(option);
                    municipios[i].value = data[i].id_dep

                }
                $('#departamento').select2();
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })

}

function cargarMunicipios() {
    document.getElementById("municipios").options.length = 0;
    return new Promise(function (resolve) {
        let idDepartamento = document.getElementById('departamento').value
        fetch('http://localhost/andrade/controlador/municipios_controlador.php?action=listarMunicipios&id_departamento=' + idDepartamento)
            .then(response => {
                return response.json()
            })
            .then(data => {

                var municipios = document.getElementById('municipios');
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    var contenido = document.createTextNode(data[i].nombre_municipio);
                    option.appendChild(contenido);
                    municipios.appendChild(option);
                    municipios[i].value = data[i].id_muni

                }
                $('#municipios').select2();
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })

}