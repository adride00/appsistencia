<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Clientes</h1>
        </div>

      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Admin Cliente</h3>

      </div>
      <div class="card-body">
        <div class="box-header with-border">
          <a class="btn btn-info" href="?ruta=nuevo_cliente">+ Nuevo cliente</a>

          <div class="resultados">

          </div>
        </div>
        <table class="table table-bordered" id="tabla_usuarios">
          <thead>
            <tr>
              <th>N°</th>
              <th>Nombre</th>
              <th>Departamento</th>
              <th>NIT</th>
              <th>NRC</th>
              <th>Telefono</th>
              <th>Direccion</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody id="contenido">
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">

      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>

<div class="modal fade" id="modalAgregarUsuario">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Agregar Nuevo Usuario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" id="formulario">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre*</label>
              <input type="text" class="form-control" id="nombre" placeholder="Ingresar Nombre" name="nombre">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Apellidos*</label>
              <input type="text" class="form-control" id="apellidos" placeholder="Ingresar Apellidos" name="apellidos">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Correo*</label>
              <input type="email" class="form-control" id="correo" placeholder="Ingresar Email" name="correo">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password*</label>
              <input type="password" class="form-control" id="pass" placeholder="Password" name="pass">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Tipo Usuario*</label>
              <select id="tipo_usuario" name="tipo_usuario" class="custom-select">
                <option>--Seleccionar Opcion--</option>
                <option value="admin">Admin</option>
                <option value="conserje">Conserje</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="guardarUsuario" class="btn btn-primary">Guardar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="mostarInfo">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
  $(function () {

    $('#tabla_usuarios').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
      }
    });
  });
</script>

<script>
  const conenido = document.querySelector('#contenido')
  window.addEventListener('load', () => {
    cargarClientes()
  })

  function cargarClientes() {
    return new Promise(function (resolve) {
      fetch('http://localhost/andrade/controlador/cliente_controlador.php?action=listarClientes')
        .then(response => {
          return response.json()
        })
        
        .then(data => {
          tabla(data) 
          resolve()

        })
        .catch(function (error) {
          console.log('Hubo un problema con la petición Fetch:' + error.message);
        });
    })
  }

  function tabla(datos) {
    for (let valor of datos) {

      contenido.innerHTML += `

            <tr>
              <th>${valor.id_cliente}</th>
              <th>${valor.nombre_cliente}</th>
              <th>${valor.nombre_departamento}</th>
              <th>${valor.nit}</th>
              <th>${valor.nrc}</th>
              <th>${valor.telefono1}</th>
              <th>${valor.direccion}</th>
              <th>
              <div class="btn-group">
                  <button type="button" class="btn btn-secondary">Opciones</button>
                  <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu">
                    <a id="editar" class="dropdown-item" href="?ruta=editarCliente&idCliente=${valor.id_cliente}">Editar</a>
                    <a id="eliminar" onClick="eliminar(${valor.id_cliente})" class="dropdown-item" href="#">Eliminar</a>
                    <a id="detalles" class="dropdown-item" href="#">Detalles</a>
                    
                  </div>
                </div>
              </th>
            </tr>

    `
    }
  };

  
</script>
<script>

  function eliminar (idCliente) {

    event.preventDefault()
    Swal.fire({
        title: 'Esta seguro?',
        text: "Este cambio es irreversible",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!'
      }).then((result) => {
        if (result.value) {

          eliminarCliente (idCliente)
        }
      })
  }

  function eliminarCliente (idCliente) {
    return new Promise(function (resolve) {
      fetch('http://localhost/andrade/controlador/cliente_controlador.php?action=eliminarCliente&idCliente='+idCliente)
        .then(response => {
          return response.json()
        })
        
        .then(data => {
          Swal.fire(
            'Registro Eliminado!',
            'Cliente ha sido eliminado',
            'success'
          )
          contenido.innerHTML = ''
          cargarClientes()
          resolve()

        })
        .catch(function (error) {
          console.log('Hubo un problema con la petición Fetch:' + error.message);
        });
    })
  }

</script>
