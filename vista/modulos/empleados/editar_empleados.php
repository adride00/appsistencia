<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ingreso de nuevo empleado</h1>
                </div>
 
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Datos del empleado</h3>

            </div>
            <div class="card-body">




                <form id="form_general" method="POST" name="fileinfo">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nombre_cliente" class="requerido">Nombre</label>
                                    <input id="nombre_empleado" name="nombre_empleado" type="text" class="form-control requeridos"
                                        id="nombre_empleado" placeholder="Nombre Empleado" required="required">
                                    <span></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="departamento" class="requerido">Apellidos</label>
                                            <input id="apellidos_empleado" name="apellidos_empleado" type="text" class="form-control requeridos"
                                         placeholder="Apellidos empleado" required="required">
                                            <span></span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nit" class="">NIT</label>
                                            <input id="nit" name="nit" type="text" class="form-control" placeholder="0000-000000-000-0">
                                            <span></span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="telefono" class="">Telefono</label>
                                            <input id="telefono" name="telefono" type="text" class="form-control tel" placeholder="0000-0000">
                                            <span></span>
                                        </div>
                                    </div>
                                   
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button id="guardar" type="submit" class="btn btn-primary">Ingresar</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="direccion">Dirección</label>
                                    <input id="direccion" name="direccion" type="text" class="form-control" id="direccion"
                                        placeholder="Dirección">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="requerido">Tipo Empleado</label>
                                            <select id="tipo_empleado" name="tipo_empleado" class="custom-select requeridos">
                                                <option value="Cajero">Cajero</option>
                                                <option value="Administrador">Administrador</option>
                                                <option value="Bodeguero">Bodeguero</option>
                                                <option value="Mecanico">Mecanico</option>
                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="dui">DUI</label>
                                            <input id="dui" name="dui" type="text" class="form-control" id="dui"
                                                placeholder="DUI">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fax">Salario</label>
                                            <input id="salario" name="salario" type="text" class="form-control" id="fax"
                                                placeholder="Salario">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Correo
                                            </label>
                                            <input id="correo" name="correo" type="email" class="form-control" placeholder="Correo">
                                        </div>
                                    </div>
                                </div>

                                <!-- /.card-body -->

                                <div class="card-footer"></div>
                            </div>

                        </div>
                </form>


            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    $('#nit').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 4);
                var bloc2 = string.substring(4, 10);
                var bloc3 = string.substring(10, 13);
                var bloc4 = string.substring(13, 13);
                var string = bloc1 + "-" + bloc2 + "-" + bloc3 + "-" + bloc4;
                $(this).val(string);
            } else {
                event.preventDefault();
            }
        }
    });

   

    $('#dui').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 8);
                var bloc2 = string.substring(8, 8);
                var string = bloc1 + "-" + bloc2;
                $(this).val(string);
            } else {
                event.preventDefault();
            }

        }
    });

    $('.tel').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 4);
                var bloc2 = string.substring(4, 7);
                var string = bloc1 + "-" + bloc2;
                $(this).val(string);
            } else {
                event.preventDefault();
            }

        }
    });
</script>
<script>

window.addEventListener('load', (event) => {
    const id = '<?php echo $_GET['id'];?>'
    event.preventDefault()
    cargar(id)
})

function cargar(id) {
    return new Promise(function (resolve) {
      fetch('http://localhost/andrade/controlador/empleados_controlador.php?action=listarEmpleados&id='+id)
        .then(response => {
          return response.json()
        })
        
        .then(data => {
            
          document.getElementById('nombre_empleado').value = data[0].nombre_empleado;
          document.getElementById('nit').value = data[0].nit;
          document.getElementById('apellidos_empleado').value = data[0].apellidos_empleado;
          document.getElementById('telefono').value = data[0].telefono;
          document.getElementById('direccion').value = data[0].direccion;
          document.getElementById('dui').value = data[0].dui;
          document.getElementById('salario').value = data[0].salario;
          document.getElementById('correo').value = data[0].correo;
          document.getElementById('tipo_empleado').value = data[0].tipo_empleado;
         
         
          resolve()

        })
        .catch(function (error) {
          console.log('Hubo un problema con la petición Fetch:' + error.message);
        });
    })
  }


document.getElementById('guardar').addEventListener('click', () => {

    
    var campos = document.querySelectorAll('.requeridos')
    var elementos = 0;
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].value == "") {

            campos[i].parentElement.lastElementChild.innerText = "Campo es requerido"
            campos[i].parentElement.lastElementChild.style = "color:red"
        } else {
            campos[i].parentElement.lastElementChild.innerText = ""

            elementos = elementos + 1;

        }

    }
    if (elementos == campos.length) {
        event.preventDefault()
        const data = new FormData(document.getElementById('form_general'));
        fetch('http://localhost/andrade/controlador/empleados_controlador.php?action=editar&id='+id, {
                method: 'POST',
                body: data
            })
            .then((response) => {
                return response.text()
            })
            .then((datos) => {
                document.getElementById('nombre_empleado').value = ''
                document.getElementById('apellidos_empleado').value = ''
                document.getElementById('direccion').value = ''
                document.getElementById('dui').value = ''
                document.getElementById('nit').value = ''
                document.getElementById('salario').value = ''
                document.getElementById('correo').value = ''
                document.getElementById('telefono').value = '' 
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Registro Guardado',
                    showConfirmButton: false,
                    timer: 1500
                  })
                 
            })
    }


})
</script>