
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>asdas</h1>
        </div>

      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">tema</h3>

      </div>
      <div class="card-body">

        <form action="" method="post" id="dataForm">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Buscar Producto</label>
                        <select style="width: 350px" name="producto" id="producto" multiple="multiple">
                            <option value="">Buscar Producto</option>
                        </select>
                        <div class="dropdown-menu" aria-labelledby="producto">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <div class="col-md6">
                    <div class="form-group m-4">
                        <button class="btn btn-warning">Ingreso</button>
                        <button type="submit" class="btn btn-success">Pagar</button>
                        <button class="btn btn-info">Borrar</button>
                        <button class="btn btn-info">Guardar</button>
                        <button class="btn btn-danger">salir</button>
                        <button class="btn btn-warning">Abrir</button>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Referencia</label>
                        <select name="referencia" id="referencia">
                            <option value="">Buscar</option>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Cliente</label>
                        <select name="cliente" id="cliente">
                            <option value="clientes_varios">Clientes Varios</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Tipo Impresion</label>
                        <select name="tipo_impresion" id="tipo_impresion">
                        <option value="F">Factura</option>
                        <option value="CF">Credito Fiscal</option>
                        <option value="FE">Factura Exportacion</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Metodo de Pago</label>
                        <select name="metodo_pago" id="tipo_impresion">
                        <option value="efectivo">Efectivo</option>
                        <option value="pago_electronico">Pago Elelctronico</option>
                        <option value="cheque">Cheque</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Tipo de Pago</label>
                        <select name="tipo_pago" id="tipo_pago">
                            <option value="contado">Contado</option>
                            <option value="credito">Credito</option>
                        </select>
                    </div>
                </div>
            </div>
        
        <input id="limite" hidden value="11" type="text">
        
        <div action="" class="row">
            <div class="col-md-8 table-responsive-lg">
                <table id="facturar" class="table table-sm">
                    <thead class="thead-dark">
                        <tr class="info">
                            <th>Descripción</th>
                            <th>Stock</th>
                            <th>Cantidad</th>
                            <th>Descuento %</th>
                            <th>Precio</th>
                            <th>Subtotal</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tfoot>
				<tr class="table-secondary">
					<td colspan="1">Pie de tabla.</td>
                    <td colspan="3">CANT. PROD</td>
                    <td id="cantidad">0</td>
                    <td>TOTALES $</td>
                    <td><input name="total" id="total" readonly style="width: 70px" type="text"></td>

				</tr>
			</tfoot>
                    <tbody id="agregarTabla">
                        
                    </tbody>
                </table>
            </div>
            <div class="col-md-4">
               <table class="table table-bordered table-sm">
                   <thead class="thead-dark">
                       <tr>
                           <th colspan="2">Pago Y Cambio</th>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>
                               <label for="">Fecha:</label>
                           </td>
                           <td>
                               <input id="fecha" type="text" readonly>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label readonly for="">Correlativo:</label>
                           </td>
                           <td>
                               <input id="correlativo" name="correlativo" type="text">
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label  for="">Total: $</label>
                           </td>
                           <td>
                               <input type="text" readonly>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label for="">N° Documento:</label>
                           </td>
                           <td>
                               <input name="numero_documento" type="text">
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label id="cliente" for="">Cliente:</label>
                           </td>
                           <td>
                               <input type="text" readonly>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label id="" for="">Direccion:</label>
                           </td>
                           <td>
                               <input readonly id="direccion" type="text">
                           </td>
                       </tr>
                       <tr id="pais" hidden readonly>
                           <td>
                               <label id="pais" for="">Pais:</label>
                           </td>
                           <td>
                               <input type="text" readonly>
                           </td>
                       </tr>
                       <tr id="ciudad" hidden>
                           <td>
                               <label  id="ciudad" for="">Ciudad:</label>
                           </td>
                           <td>
                               <input type="text" readonly>
                           </td>
                       </tr>
                       <tr id="nit" hidden>
                           <td>
                               <label for="">NIT:</label>
                           </td>
                           <td>
                               <input id="NIT" type="text" name="nit" readonly>
                           </td>
                       </tr>
                       <tr id="nrc" hidden>
                           <td>
                               <label for="">NRC:</label>
                           </td>
                           <td>
                               <input id="NRC" type="text" name="nrc" readonly>
                                <input type="text" name="categoria_cliente" id="categoria_cliente" hidden>
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label for="">Efectivo: $</label>
                           </td>
                           <td>
                               <input id="efectivo" type="text">
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <label for="">Cambio: $</label>
                           </td>
                           <td>
                               <input id="cambio" type="text" readonly>
                           </td>
                       </tr>
                   </tbody>
               </table>
            </div>
        </div>
        
        </form>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">

      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>


<script>

window.addEventListener('load', () => {
    
    $('#producto').select2();
    var numItems = $("#limite").val()
    const buscar = document.querySelector('.select2-search__field')
    var contenido = document.querySelector('#select2-producto-results')
    buscar.addEventListener('keyup', () => {
        
       //console.log(contenido)
        let producto = buscar.value
        cargarProductos(producto)

        
        
})
});



function cargarProductos(producto) {

    return new Promise(function (resolve) {
        fetch('http://localhost/andrade/controlador/facturacion_controlador.php?action=listarProductos&prod='+producto)
            .then(response => {
                return response.json()
            })
            .then(data => {

                listado(data)
                
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })

}

function listado(datos) {
    var contenido = document.querySelector('#select2-producto-results')
    for (let valor of datos) {
        var li = document.createElement("li");
        var a = document.createElement("a");
        var cont = document.createTextNode(valor.codigo + ' - ' + valor.producto);
        a.appendChild(cont);
        li.appendChild(a);
        contenido.appendChild(li);
        var id = valor.id_inventario
        a.setAttribute("onClick", 'agregar('+id+')')
        a.href = "#"
      
    }
    
  };

  function precios (){
      sumar()
      .then(() => {
          return aplicarDescuento()
      })
  }
  function agregar(id){
      event.preventDefault()
    return new Promise(function (resolve) {
        fetch('http://localhost/andrade/controlador/facturacion_controlador.php?action=listarProductos&id='+id)
            .then(response => {
                return response.json()
            })
            .then(data => {

                var producto = document.getElementById('agregarTabla')
                if(producto.childElementCount>$("#limite").val()){
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'No se pueden agregar mas articulos a la factura',
                        text: '',
                        footer: '<p>Realizar una nueva factura</p>'
                    })
                }else{
                    
                producto.insertRow(-1).innerHTML += `
                
                <tr>
                    <td>
                    <input name="producto[]" hidden value="${data[0].id_inventario}">
                    <input  readonly name="nombreProducto[]" type="text" value="${data[0].producto}">
                    </td>
                    
                    <td>
                    ${data[0].stock}
                    </td>
                    
                    <td>
                    <input id="cantidad" name="cantidad[]" class="${data[0].id_inventario}" onKeyup="this.value=sumar(${producto.childElementCount}, value, ${data[0].precio}, ${data[0].stock},this)" style="width: 90px" id="" type="text" required>
                    </td>
                    <td>
                        
                        <input maxlength="2" class="desc" name="descuento[]" onkeyup="this.value=aplicarDescuento(this)" style="width: 90px" type="text">
                    </td>
                    <td>
                    ${data[0].precio}
                    </td>
                    <td><input name="subtotal[]" class="subtotales" id="${producto.childElementCount}" style="width: 100px" type="text" readonly required></td>
                    <td hidden><input type="text" id="precio"></td>
                    <td><button onClick="eliminarFila(this)" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
                </tr>
                
                `
                
                }
                    
                
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })
  }
  var sum = 0
  function sumar(id, value, precio, stock, elemento){

    var out = '';
    var filtro = '1234567890';//Caracteres validos
	
    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<value.length; i++)
       if (filtro.indexOf(value.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
	     out += value.charAt(i);
         if(out != ''){
        var subtotal = document.getElementById(id)
        var hidden = elemento.parentNode.parentNode.cells[6].firstChild
        var sub = (parseInt(out) * parseFloat(precio)).toFixed(4);
        hidden.value = sub
        subtotal.value = sub
        
        
        sumarTotal()

        
        if(stock < out){
            Swal.fire({
            icon: 'error',
            title: '',
            text: 'No hay suficiente stock'
            
            })
            subtotal.value = ''
            elemento.value = ''
        }
      }
	
    //Retornar valor filtrado
    return out;

      
      
      
  }

  function eliminarFila (r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("facturar").deleteRow(i);
    sumarTotal ()
    $("#cambio").val('')
  }

  function sumarTotal () {
      var precioItem = $('.subtotales')
      var arraySumaPrecio = []
      for(var i = 0; i < precioItem.length; i++){
        arraySumaPrecio.push(Number($(precioItem[i]).val()))
      }
      function sumarArrayPrecios(total,numero){
        return total + numero
      }

      var sumaTotalPrecio = arraySumaPrecio.reduce(sumarArrayPrecios)
      document.getElementById('total').value = (sumaTotalPrecio).toFixed(4); 
      
  }

 
var f = new Date();
document.getElementById('fecha').value =f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();



$('#tipo_impresion').on('change', function (e) { //evento que captura el tipo de impresion, mostrara y ocultara los campos que sean necesarios
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if(valueSelected == 'CF'){
        $("#limite").val(13)
        
        $('#nit').removeAttr('hidden');
        $('#nrc').removeAttr('hidden');
    }else if(valueSelected == 'FE'){
        $("#limite").val(13) 
        $("#nit").attr("hidden",true);
        $("#nrc").attr("hidden",true);
        $('#pais').removeAttr('hidden');
        $('#ciudad').removeAttr('hidden');
    }else if(valueSelected == 'F'){
        $("#limite").val(11)
        document.getElementById("facturar").deleteRow(14);
        document.getElementById("facturar").deleteRow(13);
        $("#nit").attr("hidden",true);
        $("#nrc").attr("hidden",true);
        $("#pais").attr("hidden",true);
        $("#ciudad").attr("hidden",true);
    }
    
});



$(document).ready(function(){
    
    var clientes = $('#cliente')
    $.ajax({ //mandar peticion para cargar los clientes en el seelct
        type: "POST",
        url: "http://localhost/andrade/controlador/cliente_controlador.php?action=listarClientes",
        dataType: 'json',
        success: function(response)
        {  //agrega los clientes en el select, recorre el objeto para obtener solo los nombres y el id en el value
            $(response).each(function(i, v){ // indice, valor
                clientes.append('<option value="' + v.id_cliente + '">' + v.nombre_cliente + '</option>');
            })
            $('#cliente').select2(); //se carga la biblioteca de select 2
            $("#cliente").change(function(){ //obtener el valor del select 
                var sel = $("option:selected", this);
                var idCliente = this.value;
                if(idCliente == 'clientes_varios'){ //cajas de texto se ponen en blanco para cuando se selecciones clientes varios
                    $('#NIT').val('')
                    $('#NRC').val('')   
                    $('#direccion').val('')
                    $('#pais').val('')
                    $('#ciudad').val('')
                    $('#categoria_cliente').val('')     
                }
                $.ajax({ //peticion para llamar la demas informacion del cliente y que esta pueda estar en la facturacion
                    type: "POST",
                    url: "http://localhost/andrade/controlador/cliente_controlador.php?action=listarClientes&idCliente="+idCliente,
                    dataType: 'json',
                    success: function(r) //se carga la informacion en los lugares correspondientes
                    {
                       $('#NIT').val(r[0].nit)
                       $('#NRC').val(r[0].nrc)   
                       $('#direccion').val(r[0].direccion)
                       $('#pais').val(r[0].pais)
                       $('#ciudad').val(r[0].ciudad)
                       $('#categoria_cliente').val(r[0].categoria_cliente)
                    }
                })
            })
        }
    });
 
})

document.getElementById('efectivo').addEventListener('keyup', function(){

    $("#cambio").val(Number($("#efectivo").val() - $("#total").val()).toFixed(4))

})

function aplicarDescuento (elemento){

    var out = '';
    var filtro = '1234567890';//Caracteres validos
	
    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<elemento.value.length; i++)
       if (filtro.indexOf(elemento.value.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
            out += elemento.value.charAt(i);
            if(out<0 || out>25){
                out = ''
            }
            var subTotalHidden = elemento.parentNode.parentNode.cells[6].firstChild.value
            var subTotal = elemento.parentNode.parentNode.cells[5].firstChild
            
            var porcentaje = out
            var desc = Number(porcentaje/100)
            var descuento = Number(subTotalHidden-(subTotalHidden * desc).toFixed(4))
            subTotal.value = descuento 
            sumarTotal ()
	
    //Retornar valor filtrado
    return out;

    
    
    //console.log(subTotal)
}

$(document).submit(function(e){
    e.preventDefault()
    var producto = document.getElementById('agregarTabla')
    var numRows = producto.childElementCount
    if(numRows === 0){
        Swal.fire({
            icon: 'error',
            title: '',
            text: 'No hay productos agregados'
            
            })
    }else{
    enviarDatos()
    }
})

function enviarDatos () {
    
    const data = new FormData(document.getElementById('dataForm'));
    fetch('http://localhost/andrade/controlador/facturacion_controlador.php?action=RealizarPago', {
                method: 'POST',
                body: data
            })
            .then((response) => {
                return response.json()
            })
            .then((datos) => {
                
            })
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function getCorrelativo(){

}

</script>
