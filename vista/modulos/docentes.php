<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Docentes</h1>
        </div>

      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Admin Docentes</h3>

      </div>
      <div class="card-body">
        <div class="box-header with-border">

          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarDocente">+ Agregar Docente</button>
          <div class="resultados">

          </div>
        </div>
        <table class="table table-bordered" id="tabla_docente">
          <thead>
            <tr>
              <th>N°</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Correo</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
                $item = null;
                $valor = null;
                $equipos = ControladorDocentes::ctrMostrarDocente($item,$valor);
                foreach ($equipos as $key => $value) {
                  $numFiila = $key+1;
                echo '

                <tr style="cursor: pointer">
                <td>'.$numFiila.'</td>
                <td>'.$value["nombre"].'</td>
                <td>'.$value["apellidos"].'</td>
                <td>'.$value["correo"].'</td>
                <td class="no">
                  <div class="btn-group">
                    <button class="btn btn-warning btnEditarDocente" idDocente="'.$value["id_docente"].'" data-toggle="modal" data-target="#modalEditarDocente"><i class="fa fa-user-edit"></i></button>
                    <button class="btn btn-danger btnEliminarDocente" idDocenteEliminar="'.$value["id_docente"].'"><i class="fa fa-times"></i></button>
                    <button class="btn btn-info btnDatosEquipo" idEquipoDatos="'.$value["id_docente"].'" data-toggle="modal" data-target="#mostarInfo"><i class="fa fa-bars"></i></button>
                  </div>
                </td>
              </tr>


                ';
              }
               ?>

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">

      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>

<div class="modal fade" id="modalAgregarDocente">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Agregar Nuevo Docente</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="formulario">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre*</label>
                    <input type="text" class="form-control" id="nombre" placeholder="Ingresar Nombre" name="nombre">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Apellidos*</label>
                    <input type="text" class="form-control" id="apellidos" placeholder="Ingresar Apellidos" name="apellidos">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Correo*</label>
                    <input type="email" class="form-control" id="correo" placeholder="Ingresar Email" name="correo">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password*</label>
                    <input type="password" class="form-control" id="pass" placeholder="Password" name="pass">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>


      <div class="modal fade" id="modalEditarDocente">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Editar Docente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form role="form" id="formularioEditar">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nombre*</label>
                          <input type="text" class="form-control" id="editarNombre" placeholder="Ingresar Nombre" name="editarNombre">
                        </div>
                        <input type="hidden" name="id_docente" id="id_docente" value="">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Apellidos*</label>
                          <input type="text" class="form-control" id="editarApellidos" placeholder="Ingresar Apellidos" name="editarApellidos">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Correo*</label>
                          <input type="email" class="form-control" id="editarCorreo" placeholder="Ingresar Email" name="editarCorreo">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Password*</label>
                          <input type="text" class="form-control" id="editarPass" placeholder="Password" name="editarPass">
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="editar" class="btn btn-primary">Editar</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="mostarInfo">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Editar Docente</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                          <button type="button" id="editar" class="btn btn-primary">Editar</button>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
<script>
  $(function () {

    $('#tabla_docente').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
  });
</script>
