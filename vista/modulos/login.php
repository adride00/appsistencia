
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img style="width: 200px; margin: auto;" src="resources/img/logo2.jpeg" alt="">
    <b>Andrade</b>Pos
  </div>
  <!-- /.login-logo -->

  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Iniciar sesion</p>

      <form method="POST">
        <div class="input-group mb-3">
          <input name="usuario" type="email" class="form-control" id="usuario" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input name="pass" type="password" class="form-control" id="pass" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">

          <!-- /.col -->
          <div class="col-4">
            <button id="ingresar" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>


      <!-- /.social-auth-links -->


    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<script>

      $(function(){
        $('#ingresar').click(function(e){
          const data = {
            usuario: $('#usuario').val(),
            pass: $('#pass').val()
          }

          $.ajax({
            method: "POST",
            url: "./ajax/envio.php",
            data: data,

            success:function(res){

            if (res==1) {
              location.href = "index.php?ruta=dashboard";
            }
            else{

              swal.fire("Usuario o Contraseña Incorrecta", "", "error");
            }
          }
          });

          e.preventDefault();
        });
      });


</script>
