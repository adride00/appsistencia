<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ingreso de nuevo cliente</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Datos del Cliente</h3>

            </div>
            <div class="card-body">




                <form id="form_general" method="POST" name="fileinfo">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nombre_cliente" class="requerido">Nombre</label>
                                    <input value="" name="nombre_cliente" type="text" class="form-control requeridos"
                                        id="nombre_cliente" placeholder="Nombre Cliente" required="required">
                                    <span></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="departamento" class="requerido">Departamento</label>
                                            <select id="departamento" onchange="selecionarDep()"
                                                class="form-control requeridos" name="departamento"
                                                data-live-search="true">


                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="municipio" class="requerido">Municipio</label>
                                            <select id="municipios" class="form-control requeridos" name="municipio"
                                                data-live-search="true">


                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nit" class="requerido">NIT</label>
                                            <input value="" name="nit" type="text" class="form-control requeridos"
                                                id="nit" placeholder="0000-000000-000-0" required="required">
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nrc" class="requerido">NRC</label>
                                            <input value="" name="nrc" type="text" class="form-control requeridos"
                                                id="nrc" placeholder="NRC" required>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="telefono_1" class="requerido">Telefono 1</label>
                                            <input value="" name="telefono_1" type="text"
                                                class="form-control tel requeridos" id="telefono_1"
                                                placeholder="0000-0000" required="required">
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="telefono_2">Telefono 2
                                            </label>
                                            <input value="" name="telefono_2" type="text" class="form-control tel"
                                                id="telefono_2" placeholder="0000-0000">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button id="editar" type="submit" class="btn btn-primary">Ingresar</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="direccion">Dirección</label>
                                    <input value="" name="direccion" type="text" class="form-control" id="direccion"
                                        placeholder="Dirección">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="requerido">Categoria del Cliente</label>
                                            <select id="categoria_cliente" name="categoria_cliente" class="custom-select requeridos">
                                                <option value="Consumidor">Consumidor</option>
                                                <option value="Contribuyente">Contribuyente</option>
                                                <option value="Gran Contribuyente">Gran Contribuyente</option>
                                                <option value="Contribuyente Exento">Contribuyente Exento</option>
                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="dui">DUI</label>
                                            <input value="" name="dui" type="text" class="form-control" id="dui"
                                                placeholder="DUI">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="giro" class="requerido">Giro</label>
                                            <input value="" name="giro" type="text" class="form-control requeridos"
                                                id="giro" placeholder="Giro" required="required">
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input onclick="myFunction()" class="custom-control-input check" type="checkbox"
                                                    id="customCheckbox2">
                                                <label for="customCheckbox2"
                                                    class="custom-control-label">Retiene</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" hidden="true" id="retiene_select">
                                        <div class="form-group has-info single-line">
                                            <label>Porcentaje de Retención
                                                <span style="color:red;">*</span></label>
                                            <select name="porcentaje" class="col-md-12 select from-control" id="porcentaje">
                                                <option value="">Seleccione porcentaje</option>
                                                <option value="1">1%</option>
                                                <option value="10">10%</option>
                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fax">FAX</label>
                                            <input value="" name="fax" type="text" class="form-control" id="fax"
                                                placeholder="FAX">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Correo
                                            </label>
                                            <input id="correo" value="" name="correo" type="email" class="form-control"
                                                id="exampleInputEmail1" placeholder="Correo">
                                        </div>
                                    </div>
                                </div>

                                <!-- /.card-body -->

                                <div class="card-footer"></div>
                            </div>

                        </div>
                </form>


            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<script src="vista/js/editarCliente.js"></script>
<script type="text/javascript">
    $('#nit').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 4);
                var bloc2 = string.substring(4, 10);
                var bloc3 = string.substring(10, 13);
                var bloc4 = string.substring(13, 13);
                var string = bloc1 + "-" + bloc2 + "-" + bloc3 + "-" + bloc4;
                $(this).val(string);
            } else {
                event.preventDefault();
            }
        }
    });

    $('#nrc').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 8);
                var bloc2 = string.substring(8, 8);
                var string = bloc1 + "-" + bloc2;
                $(this).val(string);
            } else {
                event.preventDefault();
            }

        }
    });

    $('#dui').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 8);
                var bloc2 = string.substring(8, 8);
                var string = bloc1 + "-" + bloc2;
                $(this).val(string);
            } else {
                event.preventDefault();
            }

        }
    });

    $('.tel').on('keydown', function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 37 || event
            .keyCode == 39) {} else {
            if ((event.keyCode > 47 && event.keyCode < 60) || (event.keyCode > 95 && event.keyCode < 106)) {
                inputval = $(this).val();
                var string = inputval.replace(/[^0-9]/g, "");
                var bloc1 = string.substring(0, 4);
                var bloc2 = string.substring(4, 7);
                var string = bloc1 + "-" + bloc2;
                $(this).val(string);
            } else {
                event.preventDefault();
            }

        }
    });
    
    window.addEventListener('load', () => {
    idCliente = '<?php echo $_GET['idCliente'];?>'
    event.preventDefault()

    cargar()
    

});

function selecionarDep() {
    cargarMunicipios()
}

function cargar() {
    cargarDepartamentos()
        
        .then(() => {
            cargarClientes(idCliente)
        })

}

function cargarDepartamentos() {
    return new Promise(function (resolve) {
        fetch('http://localhost/andrade/controlador/municipios_controlador.php?action=listarDepartamentos')
            .then(response => {
                return response.json()
            })
            .then(data => {

                var municipios = document.getElementById('departamento');
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    var contenido = document.createTextNode(data[i].nombre_departamento);
                    option.appendChild(contenido);
                    municipios.appendChild(option);
                    municipios[i].value = data[i].id_dep

                }
                
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })

}

function cargarMunicipios() {
    document.getElementById("municipios").options.length = 0;
    return new Promise(function (resolve) {
        let idDepartamento = document.getElementById('departamento').value
        fetch('http://localhost/andrade/controlador/municipios_controlador.php?action=listarMunicipios&id_departamento=' + idDepartamento)
            .then(response => {
                return response.json()
            })
            .then(data => {

                var municipios = document.getElementById('municipios');
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    var contenido = document.createTextNode(data[i].nombre_municipio);
                    option.appendChild(contenido);
                    municipios.appendChild(option);
                    municipios[i].value = data[i].id_muni

                }
                $('#municipios').select2();
                resolve()

            })
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            });
    })

}

        
        
    


    function cargarClientes(idCliente) {
    return new Promise(function (resolve) {
      fetch('http://localhost/andrade/controlador/cliente_controlador.php?action=listarClientes&idCliente='+idCliente)
        .then(response => {
          return response.json()
        })
        
        .then(data => {
            
          document.getElementById('nombre_cliente').value = data[0].nombre_cliente;
          document.getElementById('nit').value = data[0].nit;
          document.getElementById('nrc').value = data[0].nrc;
          document.getElementById('telefono_1').value = data[0].telefono1;
          document.getElementById('telefono_2').value = data[0].telefono2;
          document.getElementById('direccion').value = data[0].direccion;
          document.getElementById('dui').value = data[0].dui;
          document.getElementById('giro').value = data[0].giro;
          document.getElementById('fax').value = data[0].fax;
          document.getElementById('correo').value = data[0].correo;
          document.getElementById('categoria_cliente').value = data[0].categoria_cliente;
         document.getElementById('departamento').value = data[0].departamento;
          document.getElementById('municipios').value = data[0].municipio;
          
          cargarMunicipios()
          $('#departamento').select2();
          
         // console.log(document.getElementById('departamento'))
          resolve()

        })
        .catch(function (error) {
          console.log('Hubo un problema con la petición Fetch:' + error.message);
        });
    })
  }
    
</script>

