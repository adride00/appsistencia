<?php
  @session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SistemaPos | ANDRADE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="vista/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="vista/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <link rel="stylesheet" href="vista/dist/css/adminlte.css">
  <link rel="stylesheet" href="vista/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <link rel="stylesheet" href="vista/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="vista/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="vista/dist/css/style.css">
  <link rel="stylesheet" href="vista/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</head>

<!-- Site wrapper -->

  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Main Sidebar Container -->


  <!-- Content Wrapper. Contains page content -->

  <?php
  if(isset($_SESSION["inicio"]) && $_SESSION["inicio"] == "ok"){
    echo '<body class="hold-transition sidebar-mini">';
    echo '<div class="wrapper">';
    include "modulos/header.php";
    include "modulos/aside.php";
    if (isset($_GET["ruta"])) {
      if(
         $_GET["ruta"] == "usuarios" ||
         
         $_GET["ruta"] == "salir" ||
         
         $_GET["ruta"] == "nuevo_cliente" ||
         $_GET["ruta"] == "editarCliente" ||
         $_GET["ruta"] == "empleados/empleados" ||
         $_GET["ruta"] == "empleados/nuevo_empleado" ||
         $_GET["ruta"] == "empleados/editar_empleados" ||
         $_GET["ruta"] == "facturacion/facturacion"){
        include "modulos/".$_GET["ruta"].".php";
      }
    }else{
      include "modulos/dashboard.php";

    }
    include "modulos/footer.php";
    echo '</div>';
}else{
  include "modulos/login.php";
}
  ?>
  <!-- /.content-wrapper -->



  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="vista/plugins/jquery/jquery.min.js"></script>
<script src="vista/plugins/typehead/typehead.js"></script>
<!-- Bootstrap 4 -->
<script src="vista/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="vista/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="vista/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="vista/js/jquery.validate.min.js" charset="utf-8"></script>

<script src="vista/plugins/toastr/toastr.min.js"></script>
<script src="vista/dist/js/demo.js"></script>
<script src="vista/plugins/datatables/jquery.dataTables.js"></script>
<script src="vista/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="vista/plugins/select2/js/select2.full.min.js"></script>


</body>
<script src="vista/js/docentes.js"></script>
<script src="vista/js/usuarios.js"></script>
<script src="vista/js/materia.js"></script>
<script src="vista/js/asignacion_docente.js"></script>


<!-- Latest compiled and minified JavaScript -->

</html>
